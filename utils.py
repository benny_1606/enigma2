from base64 import standard_b64encode
class encryption_1606_gen_key:

    def __init__(self):
        import libnacl.public
        self.k = libnacl.public.SecretKey()
    
    def get_pk(self):
        return self.k.pk
    
    def get_sk(self):
        return self.k.sk
    
    def get_pk_b64(self):
        self.k.pk_b64 = standard_b64encode(self.k.pk).decode("utf-8")
        return self.k.pk_b64
    
    def get_sk_b64(self):
        self.k.sk_b64 = standard_b64encode(self.k.sk).decode("utf-8")
        return self.k.sk_b64
        